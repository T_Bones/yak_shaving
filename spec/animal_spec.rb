require_relative "../animal.rb"

describe Animal do
	before(:each) do
		@animal = Animal.new
	end

	describe "#speak" do
		it "responds to speak" do
			@animal.should respond_to(:speak)
		end		
	end
	describe "#shave" do
		it "doesn't" do
			@animal.should_not respond_to(:shave)
		end
	end
end