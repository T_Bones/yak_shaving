require_relative "../yak.rb"

describe Yak do
	before(:each) do
		@yak = Yak.new
	end
	describe "#speak" do
		it "says something" do
			@yak.should respond_to(:speak)
		end
	end
	describe "#shave" do
		
		it "responds to shave" do
			@yak.should respond_to(:shave)
		end
		it "shaves" do
			@yak.shave.should =~ /buzz/
		end
	end
end